package recipes;

import org.bukkit.Server;
import org.bukkit.plugin.Plugin;
import recipes.recipe.SeaStoneArrow;

public class Recipes
{
    private Plugin plugin;
    private Server server;

    public Recipes(Plugin plugin)
    {
        this.plugin = plugin;
        this.server = plugin.getServer();
    }

    public void init()
    {
        IRecipe seaStoneArrow = new SeaStoneArrow();
        this.server.addRecipe(seaStoneArrow.recipe("sea_stone_arrow", seaStoneArrow.item(), this.plugin));
    }
}
