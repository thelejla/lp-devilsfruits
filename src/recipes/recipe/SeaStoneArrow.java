package recipes.recipe;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import recipes.IRecipe;

public class SeaStoneArrow implements IRecipe
{

    @Override
    public String name() {
        return ChatColor.BLUE + "Sea Stone " + ChatColor.WHITE + "Arrow";
    }

    @Override
    public ItemStack item() {
        ItemStack item = new ItemStack( Material.ARROW );
        ItemMeta im = item.getItemMeta();
        im.setDisplayName(this.name());
        item.setItemMeta( im );
        item.addUnsafeEnchantment( Enchantment.QUICK_CHARGE, 1 );
        item.addUnsafeEnchantment( Enchantment.THORNS, 1 );
        return item;
    }

    public ShapedRecipe recipe(String namespace, ItemStack item, Plugin plugin) {
        NamespacedKey key = new NamespacedKey(plugin, namespace);
        ShapedRecipe recipe = new ShapedRecipe( key, item );
        recipe.shape( " P ", " D ", " A " );
        recipe.setIngredient( 'P', Material.PRISMARINE_CRYSTALS );
        recipe.setIngredient( 'D', Material.DIAMOND );
        recipe.setIngredient( 'A', Material.ARROW );
        return recipe;
    }
}
