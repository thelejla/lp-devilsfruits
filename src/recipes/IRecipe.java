package recipes;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

public interface IRecipe
{
    public String name();

    public ItemStack item();

    public ShapedRecipe recipe(String namespace, ItemStack item, Plugin plugin);
}
