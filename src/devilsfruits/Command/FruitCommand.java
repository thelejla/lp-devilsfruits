package devilsfruits.Command;

import devilsfruits.Item.Creator.DevilFruitCreator;
import devilsfruits.Item.DevilFruit;
import devilsfruits.Item.Fruit.Logia.CandyFruit;
import devilsfruits.Item.Fruit.Logia.FireFruit;
import devilsfruits.Item.Fruits;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class FruitCommand implements CommandExecutor
{
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args)
    {
        if(!(sender instanceof Player))
        {
            return false;
        }

        Player player = (Player) sender;

        if(!player.isOp()){
            player.sendMessage(ChatColor.RED + "You are not authorized to use this command.");
            return true;
        }

        if(args.length <= 0){
            player.sendMessage(ChatColor.RED + "Could not resolve the fruit you want to claim.");
            return true;
        }

        String clazz = args[0];

        ItemStack itemStack = null;

        DevilFruit devilFruit = Fruits.getInstanceByClass(clazz);


        if(null == devilFruit){
            player.sendMessage(ChatColor.RED + "Given fruit does not exist.");
            return true;
        }

        itemStack = DevilFruitCreator.create(devilFruit);

        if(null == itemStack){
            player.sendMessage(ChatColor.RED + "Given fruit does not exist.");
            return true;
        }

        player.getInventory().addItem(itemStack);
        player.updateInventory();

        player.sendMessage(ChatColor.GREEN + "A devilfruit has been stored to your inventory.");

        return true;
    }
}
