package devilsfruits.Command;

import devilsfruits.Item.DevilFruit;
import devilsfruits.Item.Fruits;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

public class DevilExchangeCommand implements CommandExecutor
{
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args)
    {
        if(!(sender instanceof Player)){
            return false;
        }

        Player player = (Player) sender;

        ItemStack mainHand = player.getInventory().getItemInMainHand();

        if (!mainHand.getType().equals(Material.CHORUS_FRUIT))
        {
            return true;
        }

        if(null == mainHand.getItemMeta()){
            return true;
        }

        ItemMeta itemMeta = mainHand.getItemMeta();

        if(null == itemMeta.getLore()){
            return true;
        }

        List<String> lore = itemMeta.getLore();

        if(lore.size() < 3){
            return true;
        }


        String clazz = lore.get(2);

        DevilFruit fruit = Fruits.getInstanceByClass(clazz.replace(ChatColor.MAGIC+"",""));

        if(null == fruit){
            return true;
        }

        //TODO: make this more configurable
        int costs = 100;

        if(costs <= 0){
            return true;
        }

        player.getInventory().setItemInMainHand(new ItemStack(Material.AIR, 1));

        for(int i = 0;i < costs;++i){
            player.getInventory().addItem(new ItemStack(Material.GOLD_INGOT, 1));
        }

        player.updateInventory();

        return true;
    }
}
