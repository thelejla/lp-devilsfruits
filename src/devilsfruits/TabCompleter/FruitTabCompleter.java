package devilsfruits.TabCompleter;

import devilsfruits.Item.DevilFruit;
import devilsfruits.Item.Fruit.Logia.FireFruit;
import devilsfruits.Item.Fruits;
import org.bukkit.ChatColor;
import org.bukkit.block.data.type.Fire;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class FruitTabCompleter implements TabCompleter
{
    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String s, String[] args)
    {
        if(!(sender instanceof Player)){
            return null;
        }

        List<String> list = new ArrayList<>();

        List<DevilFruit> fruits = Fruits.fruits();


        for (DevilFruit devilFruit:fruits){
            list.add(devilFruit.getClass().toString().replace("class ", ""));
        }

        return list;
    }
}
