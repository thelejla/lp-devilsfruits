package devilsfruits.Item.Storage;

import devilsfruits.Item.DevilFruit;
import devilsfruits.Item.Fruits;
import org.bukkit.Bukkit;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

public class FruitOwnerStorage
{
    private String storagePath;

    public FruitOwnerStorage(String storagePath)
    {
        this.storagePath = storagePath;
    }

    public boolean store(Player player, DevilFruit devilFruit, boolean force) throws IOException, InvalidConfigurationException {
        File file = new File(this.storagePath+"/owner.yml");
        YamlConfiguration storage = new YamlConfiguration();

        if(!file.getParentFile().exists()){
            //TODO: maybe check this for results next time
            file.getParentFile().mkdirs();
            file.getParentFile().setWritable(true);
            file.getParentFile().setReadable(true);
        }

        if(!file.exists()){
            file.createNewFile();
            storage.save(file);
        }

        storage.load(file);

        if(storage.contains(devilFruit.name())){
            return false;
        }

        storage.set(devilFruit.name(), player.getUniqueId().toString());

        storage.save(file);

        return true;
    }

    public boolean remove(DevilFruit devilFruit) throws IOException, InvalidConfigurationException {
        File file = new File(this.storagePath+"/owner.yml");
        YamlConfiguration storage = new YamlConfiguration();

        if(!file.getParentFile().exists()){
            //TODO: maybe check this for results next time
            file.getParentFile().mkdirs();
            file.getParentFile().setWritable(true);
            file.getParentFile().setReadable(true);
        }

        if(!file.exists()){
            return false;
        }

        storage.load(file);

        if(!storage.contains(devilFruit.name())){
            return false;
        }

        storage.set(devilFruit.name(), null);

        storage.save(file);

        return true;
    }

    public boolean forceStore(Player player, DevilFruit devilFruit) throws IOException, InvalidConfigurationException {
        return this.store(player, devilFruit, true);
    }

    public boolean isOwned(DevilFruit devilFruit)
    {
        if(null == devilFruit){
            return false;
        }

        File file = new File(this.storagePath+"/owner.yml");
        YamlConfiguration storage = new YamlConfiguration();

        if(!file.exists()){
            return false;
        }

        try {
            storage.load(file);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
            return false;
        }

        if(!storage.contains(devilFruit.name())){
            return false;
        }

        Object playername = storage.get(devilFruit.name());

        if(null == playername){
            return false;
        }

        return true;

    }
}
