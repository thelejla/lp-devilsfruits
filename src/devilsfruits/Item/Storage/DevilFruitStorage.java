package devilsfruits.Item.Storage;

import devilsfruits.Item.DevilFruit;
import devilsfruits.Item.Fruits;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
public class DevilFruitStorage
{
    private String storagePath;

    public DevilFruitStorage(String storagePath)
    {
        this.storagePath = storagePath;
    }

    public boolean store(Player player, DevilFruit devilFruit, boolean force) throws IOException, InvalidConfigurationException {
        File file = new File(this.storagePath+"/fruits.yml");
        YamlConfiguration storage = new YamlConfiguration();

        if(!file.getParentFile().exists()){
            //TODO: maybe check this for results next time
            file.getParentFile().mkdirs();
            file.getParentFile().setWritable(true);
            file.getParentFile().setReadable(true);
        }

        if(!file.exists()){
            file.createNewFile();
            storage.save(file);
        }

        storage.load(file);

        if(storage.contains(player.getUniqueId().toString())){
            return false;
        }

        storage.set(player.getUniqueId().toString(), devilFruit.getClass().toString());

        storage.save(file);

        return true;
    }

    public boolean remove(Player player) throws IOException, InvalidConfigurationException {
        File file = new File(this.storagePath+"/fruits.yml");
        YamlConfiguration storage = new YamlConfiguration();

        if(!file.getParentFile().exists()){
            //TODO: maybe check this for results next time
            file.getParentFile().mkdirs();
            file.getParentFile().setWritable(true);
            file.getParentFile().setReadable(true);
        }

        if(!file.exists()){
            file.createNewFile();
            storage.save(file);
        }

        storage.load(file);

        if(!storage.contains(player.getUniqueId().toString())){
            return false;
        }

        storage.set(player.getUniqueId().toString(), null);

        storage.save(file);

        return true;
    }

    public boolean forceStore(Player player, DevilFruit devilFruit) throws IOException, InvalidConfigurationException {
        return this.store(player, devilFruit, true);
    }

    public DevilFruit getFruit(Player player)
    {
        if(null == player){
            return null;
        }

        File file = new File(this.storagePath+"/fruits.yml");
        YamlConfiguration storage = new YamlConfiguration();

        if(!file.exists()){
            return null;
        }

        try {
            storage.load(file);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
            return null;
        }

        if(!storage.contains(player.getUniqueId().toString())){
            return null;
        }

        Object fruit = storage.get(player.getUniqueId().toString());

        if(null == fruit){
            return null;
        }

        return Fruits.getInstanceByClass(((String)fruit).replace("class ", ""));

    }
}
