package devilsfruits.Item;

import devilsfruits.Item.Fruit.Logia.*;
import devilsfruits.Item.Fruit.Paramecia.*;
import devilsfruits.Item.Fruit.Zoan.*;
import org.bukkit.Material;
import org.bukkit.entity.Turtle;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Fruits
{
    //TODO: make this some better way
    public static List<DevilFruit> fruits()
    {
        List<DevilFruit> list = new ArrayList<>();

//        //Logia
//        list.add(new CandyFruit());
        list.add(new DarknessFruit());
        list.add(new FireFruit());
//        list.add(new FreezeFruit());
//        list.add(new GasFruit());
//        list.add(new LiquidFruit());
        list.add(new MagmaFruit());
//        list.add(new PaperFruit());
        list.add(new SandFruit());
        list.add(new SmokeFruit());
        list.add(new SnowFruit());
//        list.add(new SparkFruit());
//        list.add(new SwampFruit());
        list.add( new ThunderFruit());
//
//
//        //Zoan
//
//        list.add(new AlpacaFruit());
//        list.add(new BatFruit());
//        list.add(new BirdFruit());
//        list.add(new BullFruit());
//        list.add(new CatFruit());
//        list.add(new DogsFruit());
//        list.add(new ElephantFruit());
//        list.add(new HorsesFruit());
//        list.add(new HumaBeingHumanFruit());
//        list.add(new InsectsFruit());
//        list.add(new IronBladeFruit());
//        list.add(new KoalabearFruit());
//        list.add(new MoleFruit());
//        list.add(new SalamanderFruit());
//        list.add(new SnakesFruit());
//        list.add(new TurtlesFruit());
//
//
//        //Paramecia
//
//        list.add(new AdhesiveFruit());
//        list.add(new AimFruit());
//        list.add(new AlabasterFruit());
//        list.add(new ArtFruit());
//        list.add(new BarrierFruit());
//        list.add(new BerryFruit());
//        list.add(new BiscuitFruit());
//        list.add(new BoilFruit());
//        list.add(new BombsFruit());
//        list.add(new BooksFruit());
//        list.add(new CastleFruit());
//        list.add(new ChainFruit());
//        list.add(new ClothesFruit());
//        list.add(new ColorsFruit());
//        list.add(new CookFruit());
//        list.add(new CopyFruit());
//        list.add(new CreamFruit());
//        list.add(new CubeFruit());
//        list.add(new DearFruit());
//        list.add(new BombsFruit());
//        list.add(new EarthquakeFruit());
//        list.add(new FetterFruit());
        list.add(new FlightFruit());
//        list.add(new FloraFruit());
//        list.add(new FlutterFruit());
//        list.add(new FoamFruit());
//        list.add(new FoldingDoorFruit());
        list.add(new GhostsFruit());
//        list.add(new GlitterFruit());
//        list.add(new GoldFruit());
//        list.add(new GumGumFruit());
//        list.add(new HappinessFruit());
        list.add(new HealingFruit());
//        list.add(new HeatFruit());
//        list.add(new HobbyFruit());
//        list.add(new HormoneFruit());
//        list.add(new ImitationFruit());
        list.add(new InvisibilityFruit());
//        list.add(new JacketsFruit());
//        list.add(new KiloFruit());
//        list.add(new LameFruit());
//        list.add(new LampsFruit());
//        list.add(new LaundryFruit());
//        list.add(new MemoryFruit());
//        list.add(new MetricTonsFruit());
//        list.add(new MiniFruit());
//        list.add(new MochiFruit());
//        list.add(new MoreFruit());
//        list.add(new MotivationsFruit());
//        list.add(new MunchFruit());
//        list.add(new MushroomFruit());
//        list.add(new NeedleFruit());
//        list.add(new NetworkFruit());
//        list.add(new NitroFruit());
//        list.add(new OperationsFruit());
//        list.add(new PawsFruit());
//        list.add(new PetFruit());
//        list.add(new PlantsFruit());
//        list.add(new PlasmaFruit());
//        list.add(new PoisonFruit());
//        list.add(new ResetFruit());
//        list.add(new RockfallFruit());
//        list.add(new RotierFruit());
//        list.add(new RustFruit());
//        list.add(new SchleckFruit());
//        list.add(new SeparatedFruit());
//        list.add(new SewingFruit());
//        list.add(new ShadowFruit());
//        list.add(new ShaveFruit());
//        list.add(new ShoveFruit());
//        list.add(new SickleFruit());
//        list.add(new SilentlyFruit());
        list.add(new SleepFruit());
//        list.add(new SoulsFruit());
//        list.add(new SoundFruit());
//        list.add(new SpaceFruit());
//        list.add(new SpiralFruit());
//        list.add(new StaringFruit());
//        list.add(new StingFruit());
//        list.add(new StoneFruit());
//        list.add(new SwimmingFruit());
//        list.add(new ThreadFruit());
//        list.add(new TireFruit());
//        list.add(new TonsEmpireFruit());
//        list.add(new VoteFruit());
//        list.add(new WaxFruit());
        list.add(new WeaponsFruit());
//        list.add(new WheelFruit());
//        list.add(new WhisperFruit());
//        list.add(new WringFruit());


        return list;
    }

    //TODO: do this some better aay
    public static DevilFruit getInstanceByClass(String clazz)
    {
        clazz = "class " + clazz;
        HashMap<String, DevilFruit> fruits = new HashMap<String, DevilFruit>();

        //Logia
        fruits.put(CandyFruit.class.toString(), new CandyFruit());
        fruits.put(DarknessFruit.class.toString(), new DarknessFruit());
        fruits.put(FireFruit.class.toString(), new FireFruit());
        fruits.put(FreezeFruit.class.toString(),new FreezeFruit());
        fruits.put(GasFruit.class.toString(),new GasFruit());
        fruits.put(LiquidFruit.class.toString(),new LiquidFruit());
        fruits.put(MagmaFruit.class.toString(),new MagmaFruit());
        fruits.put(PaperFruit.class.toString(),new PaperFruit());
        fruits.put(SandFruit.class.toString(),new SandFruit());
        fruits.put(SmokeFruit.class.toString(),new SmokeFruit());
        fruits.put(SnowFruit.class.toString(),new SnowFruit());
        fruits.put(SparkFruit.class.toString(),new SparkFruit());
        fruits.put(SwampFruit.class.toString(),new SwampFruit());
        fruits.put(ThunderFruit.class.toString(),new ThunderFruit());

        //Zoan
        fruits.put(AdhesiveFruit.class.toString(),new AdhesiveFruit());
        fruits.put(AimFruit.class.toString(), new AimFruit());
        fruits.put(AlabasterFruit.class.toString(), new AlabasterFruit());
        fruits.put(ArtFruit.class.toString(),new ArtFruit());
        fruits.put(BarrierFruit.class.toString(),new BarrierFruit());
        fruits.put(BerryFruit.class.toString(),new BerryFruit());
        fruits.put(BiscuitFruit.class.toString(),new BiscuitFruit());
        fruits.put(BoilFruit.class.toString(),new BoilFruit());
        fruits.put(BombsFruit.class.toString(),new BombsFruit ());
        fruits.put(BooksFruit.class.toString(),new BooksFruit ());
        fruits.put(CastleFruit.class.toString(),new CastleFruit());
        fruits.put(KoalabearFruit.class.toString(),new KoalabearFruit());
        fruits.put(ChainFruit.class.toString(),new ChainFruit());
        fruits.put(ClothesFruit.class.toString(),new ClothesFruit());
        fruits.put(ColorsFruit.class.toString(),new ColorsFruit ());
        fruits.put(CookFruit.class.toString(),new CookFruit ());
        fruits.put(CopyFruit.class.toString(),new CopyFruit ());
        fruits.put(CreamFruit.class.toString(),new CreamFruit ());
        fruits.put(CubeFruit.class.toString(),new CubeFruit ());
        fruits.put(DearFruit.class.toString(),new DearFruit ());
        fruits.put(EarthquakeFruit.class.toString(),new EarthquakeFruit ());
        fruits.put(FetterFruit.class.toString(),new FetterFruit ());
        fruits.put(FlightFruit.class.toString(),new FlightFruit ());
        fruits.put(FloraFruit.class.toString(),new FloraFruit ());
        fruits.put(FlutterFruit.class.toString(),new FlutterFruit ());
        fruits.put(FoamFruit.class.toString(),new FoamFruit ());
        fruits.put(FoldingDoorFruit.class.toString(),new FoldingDoorFruit ());
        fruits.put(GhostsFruit.class.toString(),new GhostsFruit ());
        fruits.put(GlitterFruit.class.toString(),new GlitterFruit ());
        fruits.put(GoldFruit.class.toString(),new GoldFruit ());
        fruits.put(GumGumFruit.class.toString(),new GumGumFruit ());
        fruits.put(HappinessFruit.class.toString(),new HappinessFruit ());
        fruits.put(HealingFruit.class.toString(),new HealingFruit ());
        fruits.put(HeatFruit.class.toString(),new HeatFruit ());
        fruits.put(HobbyFruit.class.toString(),new HobbyFruit ());
        fruits.put(HormoneFruit.class.toString(),new HormoneFruit ());
        fruits.put(ImitationFruit.class.toString(),new ImitationFruit ());
        fruits.put(InvisibilityFruit.class.toString(),new InvisibilityFruit ());
        fruits.put(JacketsFruit.class.toString(),new JacketsFruit ());
        fruits.put(KiloFruit.class.toString(),new KiloFruit ());
        fruits.put(LameFruit.class.toString(),new LameFruit ());
        fruits.put(LampsFruit.class.toString(),new LampsFruit ());
        fruits.put(LaundryFruit.class.toString(),new LaundryFruit ());
        fruits.put(MemoryFruit.class.toString(),new MemoryFruit ());
        fruits.put(MetricTonsFruit.class.toString(),new MetricTonsFruit ());
        fruits.put(MiniFruit.class.toString(),new MiniFruit ());
        fruits.put(MirrorFruit.class.toString(),new MirrorFruit ());
        fruits.put(MixedFruit.class.toString(),new MixedFruit ());
        fruits.put(MochiFruit.class.toString(),new MochiFruit ());
        fruits.put(MoreFruit.class.toString(),new MoreFruit ());
        fruits.put(MotivationsFruit.class.toString(),new MotivationsFruit ());
        fruits.put(MunchFruit.class.toString(),new MunchFruit ());
        fruits.put(MushroomFruit.class.toString(),new MushroomFruit ());
        fruits.put(NeedleFruit.class.toString(),new NeedleFruit ());
        fruits.put(NetworkFruit.class.toString(),new NetworkFruit ());
        fruits.put(NitroFruit.class.toString(),new NitroFruit ());
        fruits.put(OperationsFruit.class.toString(),new OperationsFruit ());
        fruits.put(PawsFruit.class.toString(),new PawsFruit ());
        fruits.put(PetFruit.class.toString(),new PetFruit ());
        fruits.put(PlantsFruit.class.toString(),new PlantsFruit ());
        fruits.put(PlasmaFruit.class.toString(),new PlasmaFruit ());
        fruits.put(PoisonFruit.class.toString(),new PoisonFruit ());
        fruits.put(PoundingFruit.class.toString(),new PoundingFruit ());
        fruits.put(ResetFruit.class.toString(),new ResetFruit ());
        fruits.put(RockfallFruit.class.toString(),new RockfallFruit ());
        fruits.put(RotierFruit.class.toString(),new RotierFruit ());
        fruits.put(RustFruit.class.toString(),new RustFruit ());
        fruits.put(SchleckFruit.class.toString(),new SchleckFruit ());
        fruits.put(SeparatedFruit.class.toString(),new SeparatedFruit ());
        fruits.put(SewingFruit.class.toString(),new SewingFruit ());
        fruits.put(ShadowFruit.class.toString(),new ShadowFruit ());
        fruits.put(ShaveFruit.class.toString(),new ShaveFruit ());
        fruits.put(ShoveFruit.class.toString(),new ShoveFruit ());
        fruits.put(SickleFruit.class.toString(),new SickleFruit ());
        fruits.put(SilentlyFruit.class.toString(),new SilentlyFruit ());
        fruits.put(SleepFruit.class.toString(),new SleepFruit ());
        fruits.put(SoulsFruit.class.toString(),new SoulsFruit ());
        fruits.put(SoundFruit.class.toString(),new SoundFruit ());
        fruits.put(SpaceFruit.class.toString(),new SpaceFruit ());
        fruits.put(SpiralFruit.class.toString(),new SpiralFruit ());
        fruits.put(StaringFruit.class.toString(),new StaringFruit ());
        fruits.put(StingFruit.class.toString(),new StingFruit ());
        fruits.put(StoneFruit.class.toString(),new StoneFruit ());
        fruits.put(SwimmingFruit.class.toString(),new SwimmingFruit ());
        fruits.put(ThreadFruit.class.toString(),new ThreadFruit ());
        fruits.put(TireFruit.class.toString(),new TireFruit ());
        fruits.put(TonsEmpireFruit.class.toString(),new TonsEmpireFruit ());
        fruits.put(VoteFruit.class.toString(),new VoteFruit ());
        fruits.put(WaxFruit.class.toString(),new WaxFruit ());
        fruits.put(WeaponsFruit.class.toString(),new WeaponsFruit ());
        fruits.put(WheelFruit.class.toString(),new WheelFruit ());
        fruits.put(WhisperFruit.class.toString(),new WhisperFruit ());
        fruits.put(WringFruit.class.toString(),new WringFruit ());

        //Zoan
        fruits.put(AlpacaFruit.class.toString(),new AlpacaFruit ());
        fruits.put(BatFruit.class.toString(),new BatFruit ());
        fruits.put(BirdFruit.class.toString(),new BirdFruit ());
        fruits.put(BullFruit.class.toString(),new BullFruit ());
        fruits.put(CatFruit.class.toString(),new CatFruit ());
        fruits.put(DogsFruit.class.toString(),new DogsFruit ());
        fruits.put(ElephantFruit.class.toString(),new ElephantFruit ());
        fruits.put(HorsesFruit.class.toString(),new HorsesFruit ());
        fruits.put(HumaBeingHumanFruit.class.toString(),new HumaBeingHumanFruit ());
        fruits.put(InsectsFruit.class.toString(),new InsectsFruit ());
        fruits.put(IronBladeFruit.class.toString(),new IronBladeFruit ());
        fruits.put(KoalabearFruit.class.toString(),new KoalabearFruit ());
        fruits.put(MoleFruit.class.toString(),new MoleFruit ());
        fruits.put(SalamanderFruit.class.toString(),new SalamanderFruit ());
        fruits.put(SnakesFruit.class.toString(),new SnakesFruit ());
        fruits.put(TurtlesFruit.class.toString(),new TurtlesFruit ());


        if(!fruits.containsKey(clazz)){
            return null;
        }

        return fruits.get(clazz);
    }

    public static DevilFruit getFruitByItem(ItemStack item) {
        if (!item.getType().equals(Material.CHORUS_FRUIT)) {
            return null;
        }

        ItemMeta itemMeta = item.getItemMeta();

        if (null == itemMeta) {
            return null;
        }

        List<String> lore = itemMeta.getLore();

        if (null == lore) {
            return null;
        }

        if (null == lore.get(0) || null == lore.get(1) || null == lore.get(2)) {
            return null;
        }

        String devilFruitClass = DevilFruit.getClassFromMagicString(lore.get(2));

        DevilFruit devilFruit = Fruits.getInstanceByClass(devilFruitClass);

        return devilFruit;
    }
}
