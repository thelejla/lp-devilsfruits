package devilsfruits.Item;

import devilsfruits.Item.Element.DevilFruitElement;
import devilsfruits.Item.Grade.DevilFruitGrade;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public interface DevilFruit
{
    public String name();

    public DevilFruitElement element();

    public DevilFruitGrade grade();

    public void effect(Player player, Block block);


    public default boolean effectAtEntity(Player player, Entity entity) {
        return false;
    }

    public int strength();

    public default int price()
    {
        return 0;
    }

    public static String getClassFromMagicString(String name)
    {
        return name.replace(ChatColor.MAGIC + "", "");
    }

    public default boolean physicalImmunity()
    {
        return false;
    }

    public default boolean move(Player player)
    {
        return false;
    }

    public default boolean projectileReaction()
    {
        return false;
    }

}
