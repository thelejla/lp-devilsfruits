package devilsfruits.Item.Element;

import org.bukkit.ChatColor;

import java.util.List;

public interface DevilFruitElement
{
    public String name();

    public ChatColor color();

    public List<DevilFruitElement> weakness();

    public List<DevilFruitElement> strongness();
}
