package devilsfruits.Item.Element;

import org.bukkit.ChatColor;

import java.util.List;

public class ZoanElement implements DevilFruitElement
{
    @Override
    public String name() {
        return "Zoan";
    }

    @Override
    public ChatColor color() {
        return ChatColor.YELLOW;
    }

    @Override
    public List<DevilFruitElement> weakness() {
        return null;
    }

    @Override
    public List<DevilFruitElement> strongness() {
        return null;
    }
}
