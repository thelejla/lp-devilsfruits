package devilsfruits.Item.Element;

import org.bukkit.ChatColor;

import java.util.List;

public class ParameciaElement implements DevilFruitElement
{
    @Override
    public String name() {
        return "Paramecia";
    }

    @Override
    public ChatColor color() {
        return ChatColor.LIGHT_PURPLE;
    }

    @Override
    public List<DevilFruitElement> weakness() {
        return null;
    }

    @Override
    public List<DevilFruitElement> strongness() {
        return null;
    }
}
