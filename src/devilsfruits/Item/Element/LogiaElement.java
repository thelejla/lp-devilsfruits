package devilsfruits.Item.Element;

import org.bukkit.ChatColor;

import java.util.Collections;
import java.util.List;

public class LogiaElement implements DevilFruitElement
{
    @Override
    public String name() {
        return "Logia";
    }

    @Override
    public ChatColor color() {
        return ChatColor.RED;
    }

    @Override
    public List<DevilFruitElement> weakness() {
        return Collections.emptyList();
    }

    @Override
    public List<DevilFruitElement> strongness() {
        return Collections.emptyList();
    }
}
