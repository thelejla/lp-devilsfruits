package devilsfruits.Item.Creator;

import devilsfruits.Item.DevilFruit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class DevilFruitCreator
{
    public static ItemStack create(DevilFruit devilFruit)
    {
        ItemStack itemStack = new ItemStack(Material.CHORUS_FRUIT, 1);
        if(null == itemStack.getItemMeta()){
            ItemMeta im = new ItemStack(Material.SPLASH_POTION, 1).getItemMeta();
            itemStack.setItemMeta(im);
        }

        ItemMeta itemMeta = itemStack.getItemMeta();

        String devilFruitName = devilFruit.element().color() + devilFruit.name();

        itemMeta.setDisplayName(devilFruitName);

        List<String> lore = itemMeta.getLore();

        if(null == lore){
            lore = new ArrayList<>();
        }


        String gradeString = ChatColor.GRAY + " 4 Grade";

        if(null != devilFruit.grade()) {
            int grade = devilFruit.grade().grade();
            switch (grade) {
                case 1:
                    gradeString = ChatColor.GOLD + " " + grade + " Grade";
                    break;
                case 2:
                    gradeString = ChatColor.LIGHT_PURPLE + " " + grade + " Grade";
                    break;
                case 3:
                    gradeString = ChatColor.AQUA + " " + grade + " Grade";
                    break;
            }
        }



        String element = " " + devilFruit.element().color() + devilFruit.element().name();

        lore.add(element);

        lore.add(gradeString);

        lore.add(ChatColor.MAGIC + "" + devilFruit.getClass().toString().replace("class ", ""));

        itemMeta.setLore(lore);
        itemMeta.addEnchant(Enchantment.QUICK_CHARGE, 1, true);

        itemStack.setItemMeta(itemMeta);

        return itemStack;
    }
}
