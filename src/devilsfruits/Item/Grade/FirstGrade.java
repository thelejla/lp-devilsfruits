package devilsfruits.Item.Grade;

public class FirstGrade implements DevilFruitGrade
{
    @Override
    public int grade() {
        return 1;
    }
}
