package devilsfruits.Item.Grade;

public class SecondGrade implements DevilFruitGrade
{
    @Override
    public int grade() {
        return 2;
    }
}
