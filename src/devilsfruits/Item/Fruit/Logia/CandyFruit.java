package devilsfruits.Item.Fruit.Logia;

import devilsfruits.Item.DevilFruit;
import devilsfruits.Item.Element.DevilFruitElement;
import devilsfruits.Item.Element.LogiaElement;
import devilsfruits.Item.Grade.DevilFruitGrade;
import devilsfruits.Item.Grade.ThirdGrade;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.ShulkerBullet;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Random;

public class CandyFruit implements DevilFruit
{
    @Override
    public String name() {
        return "Candy Fruit";
    }

    @Override
    public DevilFruitElement element() {
        return new LogiaElement();
    }

    @Override
    public DevilFruitGrade grade() {
        return new ThirdGrade();
    }

    @Override
    public void effect(Player player, Block block) {
        int slot = player.getInventory().getHeldItemSlot();

        int level = player.getLevel() + 1;

        if (level > 50) {
            level = 50;
        }

        switch (slot) {
            case 0:
                int nLevel = level > 30 ? 30 : level;

                float radius = 0.5F;

                Location location = player.getEyeLocation();
                location.setDirection(location.getDirection().multiply(20));

                player.getWorld().createExplosion(location, (radius * nLevel));

                player.setLastDamage(0);
                break;
        }
    }

    @Override
    public int strength() {
        return 2;
    }
}
