package devilsfruits.Item.Fruit.Logia;

import devilsfruits.Item.DevilFruit;
import devilsfruits.Item.Element.DevilFruitElement;
import devilsfruits.Item.Element.LogiaElement;
import devilsfruits.Item.Grade.DevilFruitGrade;
import devilsfruits.Item.Grade.FirstGrade;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class ThunderFruit implements DevilFruit
{
    @Override
    public String name() {
        return "Thunder Fruit";
    }

    @Override
    public DevilFruitElement element() {
        return new LogiaElement();
    }

    @Override
    public DevilFruitGrade grade() {
        return new FirstGrade();
    }

    @Override
    public void effect(Player player, Block block) {
        if(!player.getInventory().getItemInMainHand().getType().equals(Material.AIR)){
            return;
        }

        int level = player.getLevel();

        if(level > 100){
            level = 100;
        }

        int slot = player.getInventory().getHeldItemSlot();

        switch (slot){
            case 0:
                Location location = player.getLocation().clone();

                player.getWorld().strikeLightning(location);

                player.setVelocity(player.getLocation().getDirection().multiply(-3.0F));
                player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_FALLING, 200, 50));
                player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 200, 50));
                break;
            case 1:
                int i = 0;
                for(Entity nearEntity:player.getNearbyEntities(player.getLocation().getX(), player.getLocation().getY(), player.getLocation().getZ())){
                    if(nearEntity.getUniqueId().toString().equals(player.getUniqueId().toString())){
                        continue;
                    }

                    if(i > player.getLevel()/5){
                        return;
                    }

                    player.getWorld().strikeLightning(nearEntity.getLocation());
                    ++i;
                }
            case 2:
                for(int y = 0;y < level;++y){
                    Location jumpLocation = player.getLocation().clone();
                    jumpLocation.setY(jumpLocation.getY()+y);

                    if(y%2 > 0){
                        jumpLocation.getBlock().setType(Material.FIRE);
                    }else{
                        jumpLocation.getBlock().setType(Material.TNT);
                    }
                }

                Location teleportLocation = player.getLocation().clone();
                teleportLocation.setY(teleportLocation.getY()+level+1);
                teleportLocation.getBlock().setType(Material.GOLD_BLOCK);
                teleportLocation.setY(teleportLocation.getY()+1);
                player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_FALLING, 100, 25));
                player.teleport(teleportLocation);
                break;
        }
    }

    @Override
    public int strength() {
        return 9;
    }
}
