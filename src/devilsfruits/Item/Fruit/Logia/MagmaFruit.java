package devilsfruits.Item.Fruit.Logia;

import devilsfruits.Item.DevilFruit;
import devilsfruits.Item.Element.DevilFruitElement;
import devilsfruits.Item.Element.LogiaElement;
import devilsfruits.Item.Grade.DevilFruitGrade;
import devilsfruits.Item.Grade.FirstGrade;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Random;

public class MagmaFruit implements DevilFruit
{
    @Override
    public String name() {
        return "Magma Fruit";
    }

    @Override
    public DevilFruitElement element() {
        return new LogiaElement();
    }

    @Override
    public DevilFruitGrade grade() {
        return new FirstGrade();
    }

    @Override
    public void effect(Player player, Block block)
    {
        int slot = player.getInventory().getHeldItemSlot();

        int level = player.getLevel()+1;

        if(level > 50){
            level = 50;
        }

        switch (slot){
            case 0:
                for(int i = -3;i < ((level/2)+1);++i){
                    Location locationX = player.getLocation().clone();
                    locationX.setX(locationX.getX()+i);
                    locationX.getBlock().setType(Material.LAVA);
                }

                for(int i = -3;i < 3;++i){
                    Location locationZ = player.getLocation().clone();
                    locationZ.setZ(locationZ.getZ()+i);
                    locationZ.getBlock().setType(Material.LAVA);

                }

                break;
            case 1:
                for(int i = -3;i < ((level/2)+1);++i){
                    Location locationX = player.getLocation().clone();
                    locationX.setX(locationX.getX()+i);
                    if(
                            !locationX.getBlock().getType().equals(Material.AIR) &&
                                    !locationX.getBlock().getType().equals(Material.GRASS) &&
                                    !locationX.getBlock().getType().equals(Material.FERN) &&
                                    !locationX.getBlock().getType().equals(Material.BROWN_MUSHROOM) &&
                                    !locationX.getBlock().getType().equals(Material.RED_MUSHROOM) &&
                                    !locationX.getBlock().getType().equals(Material.SUNFLOWER) &&
                                    !locationX.getBlock().getType().equals(Material.SNOW) &&
                                    !locationX.getBlock().getType().equals(Material.SNOW_BLOCK) &&
                                    !locationX.getBlock().getType().toString().contains("_TULIP")
                    ){
                        continue;
                    }
                    locationX.getBlock().setType(Material.MAGMA_BLOCK);
                }

                for(int i = -3;i < 3;++i){
                    Location locationZ = player.getLocation().clone();
                    locationZ.setZ(locationZ.getZ()+i);
                    if(
                            !locationZ.getBlock().getType().equals(Material.AIR) &&
                                    !locationZ.getBlock().getType().equals(Material.GRASS) &&
                                    !locationZ.getBlock().getType().equals(Material.FERN) &&
                                    !locationZ.getBlock().getType().equals(Material.BROWN_MUSHROOM) &&
                                    !locationZ.getBlock().getType().equals(Material.RED_MUSHROOM) &&
                                    !locationZ.getBlock().getType().equals(Material.SUNFLOWER) &&
                                    !locationZ.getBlock().getType().equals(Material.SNOW) &&
                                    !locationZ.getBlock().getType().equals(Material.SNOW_BLOCK) &&
                                    !locationZ.getBlock().getType().toString().contains("_TULIP")
                    ){
                        continue;
                    }
                    locationZ.getBlock().setType(Material.MAGMA_BLOCK);

                }

                break;
            case 2:
                for(int y = 0;y < level;++y){
                    Location jumpLocation = player.getLocation().clone();
                    jumpLocation.setY(jumpLocation.getY()+y);

                    if(y%2 > 0){
                        jumpLocation.getBlock().setType(Material.FIRE);
                    }else{
                        jumpLocation.getBlock().setType(Material.MAGMA_BLOCK);
                    }
                }

                Location teleportLocation = player.getLocation().clone();
                teleportLocation.setY(teleportLocation.getY()+level+1);
                player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_FALLING, 100, 25));
                player.teleport(teleportLocation);
                break;
        }
    }

    @Override
    public int strength() {
        return 10;
    }
}
