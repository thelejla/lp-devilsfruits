package devilsfruits.Item.Fruit.Logia;

import devilsfruits.Item.DevilFruit;
import devilsfruits.Item.Element.DevilFruitElement;
import devilsfruits.Item.Element.LogiaElement;
import devilsfruits.Item.Grade.DevilFruitGrade;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Random;

public class SandFruit implements DevilFruit {
    @Override
    public String name() {
        return "Sand Fruit";
    }

    @Override
    public DevilFruitElement element() {
        return new LogiaElement();
    }

    @Override
    public DevilFruitGrade grade() {
        return null;
    }

    @Override
    public void effect(Player player, Block block)
    {
        int slot = player.getInventory().getHeldItemSlot();

        int level = player.getLevel()+1;

        if(level > 50){
            level = 50;
        }

        switch (slot){
            case 0:
                for(int i = 0;i < level;++i){
                    Location location = player.getLocation().clone();
                    location.setY(location.getY()+1+(i*2));
                    location.setX(location.getX()+(new Random()).nextInt(10));
                    location.setZ(location.getZ()+(new Random()).nextInt(10));
                    Entity snowball = player.getWorld().spawnEntity(location, EntityType.FALLING_BLOCK);
                    snowball.setVelocity(player.getLocation().getDirection().multiply(10.0F));
                    ShulkerBullet bullet = (ShulkerBullet) player.getWorld().spawnEntity(location, EntityType.SHULKER_BULLET);
                    bullet.setVelocity(player.getLocation().getDirection().multiply(10.0F));
                    bullet.setSilent(true);
                }
                for(int i = 0;i < level;++i){
                    Location location = player.getLocation().clone();
                    location.setY(location.getY()+1+(i*2));
                    location.setX(location.getX()+(new Random()).nextInt(10));
                    location.setZ(location.getZ()+(new Random()).nextInt(10));
                    Entity snowball = player.getWorld().spawnEntity(location, EntityType.FALLING_BLOCK);
                    snowball.setVelocity(player.getLocation().getDirection().multiply(10.0F));
                    ShulkerBullet bullet = (ShulkerBullet) player.getWorld().spawnEntity(location, EntityType.SHULKER_BULLET);
                    bullet.setVelocity(player.getLocation().getDirection().multiply(10.0F));
                    bullet.setSilent(true);
                }
                break;
            case 1:
                int firstLevel = level/4;
                for(int i = -level;i < level;++i){
                    Location locationX = player.getLocation().clone();
                    locationX.setY(locationX.getY()+10);
                    locationX.setX(locationX.getX()+i);
                    if(!locationX.getBlock().getType().equals(Material.AIR)){
                        continue;
                    }
                    locationX.getBlock().setType(Material.SAND);
                }

                for(int i = -level;i < level;++i){
                    Location locationZ = player.getLocation().clone();
                    locationZ.setY(locationZ.getY()+10);
                    locationZ.setZ(locationZ.getZ()+i);
                    if(!locationZ.getBlock().getType().equals(Material.AIR)){
                        continue;
                    }
                    locationZ.getBlock().setType(Material.SAND);

                }


                Location teleportLocation = player.getLocation().clone();
                teleportLocation.setY(teleportLocation.getY()+11);
                player.teleport(teleportLocation);

                break;
            case 2:
                int nLevel = level/2;
                Random r = new Random();
                for(int y = -nLevel;y < nLevel;y += 2){
                    for(int x = -nLevel;x < nLevel;x += 2){
                        for(int z = -nLevel;z < nLevel;z += 2){
                            Location stormLocation = player.getLocation().clone();
                            if(r.nextInt(100) > 50){
                                continue;
                            }
                            stormLocation.setX(stormLocation.getX()+x);
                            stormLocation.setY(stormLocation.getY()+y);
                            stormLocation.setZ(stormLocation.getZ()+z);

                            stormLocation.getBlock().setType(Material.SAND);
                        }
                    }
                }
                break;
            case 3:
                Location teleportSandLocation = player.getLocation().clone();
                teleportSandLocation.setY(teleportSandLocation.getY()-1);

                if(
                        !teleportSandLocation.getBlock().getType().equals(Material.SAND) &&
                                !teleportSandLocation.getBlock().getType().equals(Material.SANDSTONE) &&
                                !teleportSandLocation.getBlock().getType().equals(Material.SANDSTONE_SLAB) &&
                                !teleportSandLocation.getBlock().getType().equals(Material.SOUL_SAND) &&
                                !teleportSandLocation.getBlock().getType().equals(Material.RED_SAND)
                ){
                    return;
                }

                Location teleportOnSandLocation = player.getLocation().clone();
                player.setVelocity(player.getLocation().getDirection().multiply(10.0F));
                break;
            case 4:
                Location cloudLocation = player.getLocation().clone();
                cloudLocation.setY(cloudLocation.getY()+1);
                player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 250, 50));
                break;
            case 5:
                if(player.getAllowFlight()){
                    player.setAllowFlight(false);
                    player.setFlying(false);
                    player.sendMessage(ChatColor.RED + "Flying deactivated");
                    player.removePotionEffect(PotionEffectType.INVISIBILITY);
                    return;
                }

                int level2 = player.getLevel();
                float speed = 0.2F;

                player.sendMessage(""+speed);

                player.setAllowFlight(true);
                player.setFlySpeed(speed);
                player.setFlying(true);
                player.sendMessage(ChatColor.GREEN + "Flying activated");
                player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 1000, 50));
                break;
        }
    }

    @Override
    public int strength() {
        return 10;
    }
}
