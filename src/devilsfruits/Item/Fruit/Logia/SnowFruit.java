package devilsfruits.Item.Fruit.Logia;

import devilsfruits.Item.DevilFruit;
import devilsfruits.Item.Element.DevilFruitElement;
import devilsfruits.Item.Element.LogiaElement;
import devilsfruits.Item.Grade.DevilFruitGrade;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.*;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import java.util.Random;

public class SnowFruit implements DevilFruit {
    @Override
    public String name() {
        return "Snow Fruit";
    }

    @Override
    public DevilFruitElement element() {
        return new LogiaElement();
    }

    @Override
    public DevilFruitGrade grade() {
        return null;
    }

    @Override
    public void effect(Player player, Block block) {
        int slot = player.getInventory().getHeldItemSlot();

        int level = player.getLevel()+1;

        if(level > 50){
            level = 50;
        }

        switch (slot){
            case 0:
                for(int i = 0;i < level;++i){
                    Location location = player.getLocation().clone();
                    location.setY(location.getY()+1+(i*2));
                    location.setX(location.getX()+(new Random()).nextInt(10));
                    location.setZ(location.getZ()+(new Random()).nextInt(10));
                    Entity snowball = player.getWorld().spawnEntity(location, EntityType.SNOWBALL);
                    snowball.setVelocity(player.getLocation().getDirection().multiply(10.0F));
                    ShulkerBullet bullet = (ShulkerBullet) player.getWorld().spawnEntity(location, EntityType.SHULKER_BULLET);
                    bullet.setVelocity(player.getLocation().getDirection().multiply(10.0F));
                    bullet.setSilent(true);
                }
                for(int i = 0;i < level;++i){
                    Location location = player.getLocation().clone();
                    location.setY(location.getY()+1+(i*2));
                    location.setX(location.getX()+(new Random()).nextInt(10));
                    location.setZ(location.getZ()+(new Random()).nextInt(10));
                    Entity snowball = player.getWorld().spawnEntity(location, EntityType.SNOWBALL);
                    snowball.setVelocity(player.getLocation().getDirection().multiply(10.0F));
                    ShulkerBullet bullet = (ShulkerBullet) player.getWorld().spawnEntity(location, EntityType.SHULKER_BULLET);
                    bullet.setSilent(true);
                }
                break;
            case 1:
                if(level > 0){
                    Location location1 = player.getLocation().clone();
                    location1.setX(location1.getX()+3);
                    player.getWorld().spawnEntity(location1, EntityType.SNOWMAN);

                }

                if(level > 10){
                    Location location2 = player.getLocation().clone();
                    location2.setX(location2.getX()-3);
                    player.getWorld().spawnEntity(location2, EntityType.SNOWMAN);
                }

                if(level > 20){
                    Location location3 = player.getLocation().clone();
                    location3.setZ(location3.getZ()-3);
                    player.getWorld().spawnEntity(location3, EntityType.SNOWMAN);
                }

                if(level > 30){
                    Location location1 = player.getLocation().clone();
                    location1.setX(location1.getX()+3);
                    player.getWorld().spawnEntity(location1, EntityType.SNOWMAN);

                    Location location2 = player.getLocation().clone();
                    location2.setX(location2.getX()-3);
                    player.getWorld().spawnEntity(location2, EntityType.SNOWMAN);

                    Location location3 = player.getLocation().clone();
                    location3.setZ(location3.getZ()-3);
                    player.getWorld().spawnEntity(location3, EntityType.SNOWMAN);

                    Location location4 = player.getLocation().clone();
                    location4.setX(location4.getZ()+3);
                    player.getWorld().spawnEntity(location4, EntityType.SNOWMAN);
                }
                break;
            case 2:
                for(int y = 0;y < level;++y){
                    Location jumpLocation = player.getLocation().clone();
                    jumpLocation.setY(jumpLocation.getY()+y);

                    if(y%2 > 0){
                        jumpLocation.getBlock().setType(Material.FIRE);
                    }else{
                        jumpLocation.getBlock().setType(Material.SNOW);
                    }
                }

                Location teleportLocation = player.getLocation().clone();
                teleportLocation.setY(teleportLocation.getY()+level+1);
                teleportLocation.getBlock().setType(Material.IRON_BLOCK);
                teleportLocation.setY(teleportLocation.getY()+1);
                player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_FALLING, 100, 25));
                player.teleport(teleportLocation);
                break;
            case 3:
                Location teleportSnowLocation = player.getLocation().clone();
                teleportSnowLocation.setY(teleportSnowLocation.getY()-1);

                if(
                                !teleportSnowLocation.getBlock().getType().equals(Material.ICE) &&
                                !teleportSnowLocation.getBlock().getType().equals(Material.BLUE_ICE) &&
                                !teleportSnowLocation.getBlock().getType().equals(Material.FROSTED_ICE) &&
                                !teleportSnowLocation.getBlock().getType().equals(Material.PACKED_ICE) &&
                                !teleportSnowLocation.getBlock().getType().equals(Material.SNOW) &&
                                !teleportSnowLocation.getBlock().getType().equals(Material.SNOW_BLOCK)
                ){
                    return;
                }

                player.setVelocity(player.getLocation().getDirection().multiply(10.0F));
                break;
            case 4:
                Location location = player.getLocation().clone();
                location.setY(location.getY()+3);
                location.setX(location.getX()+(new Random()).nextInt(10));
                location.setZ(location.getZ()+(new Random()).nextInt(10));
                Entity snowball = player.getWorld().spawnEntity(location, EntityType.SNOWBALL);
                snowball.setVelocity(player.getLocation().getDirection().multiply(1.0F));
                snowball.addPassenger(player);
        }
    }

    @Override
    public int strength() {
        return 11;
    }
}
