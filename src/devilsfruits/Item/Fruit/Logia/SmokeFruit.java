package devilsfruits.Item.Fruit.Logia;

import devilsfruits.Item.DevilFruit;
import devilsfruits.Item.Element.DevilFruitElement;
import devilsfruits.Item.Element.LogiaElement;
import devilsfruits.Item.Grade.DevilFruitGrade;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.*;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.projectiles.ProjectileSource;

import java.util.Random;

public class SmokeFruit implements DevilFruit {
    @Override
    public String name() {
        return "Smoke Fruit";
    }

    @Override
    public DevilFruitElement element() {
        return new LogiaElement();
    }

    @Override
    public DevilFruitGrade grade() {
        return null;
    }

    @Override
    public void effect(Player player, Block block)
    {
        int slot = player.getInventory().getHeldItemSlot();

        int level = player.getLevel()+1;

        if(level > 50){
            level = 50;
        }

        switch (slot){
            case 0:
                for(int i = 0;i < level;++i){
                    Location location = player.getLocation().clone();
                    location.setY(location.getY()+1);
                    location.setX(location.getX()+(new Random()).nextInt(10));
                    location.setZ(location.getZ()+(new Random()).nextInt(10));
                    ShulkerBullet bullet = (ShulkerBullet) player.getWorld().spawnEntity(location, EntityType.SHULKER_BULLET);
                    bullet.setVelocity(player.getLocation().getDirection().multiply(10.0F));
                    bullet.setSilent(true);

                }
                for(int i = 0;i < level;++i){
                    Location location = player.getLocation().clone();
                    location.setY(location.getY()+1);
                    location.setX(location.getX()+(new Random()).nextInt(10));
                    location.setZ(location.getZ()+(new Random()).nextInt(10));
                    ShulkerBullet bullet = (ShulkerBullet) player.getWorld().spawnEntity(location, EntityType.SHULKER_BULLET);
                    bullet.setVelocity(player.getLocation().getDirection().multiply(10.0F));
                    bullet.setSilent(true);

                }
                break;
            case 1:
                int nLevel = level > 30 ? 30 : level;

                float radius = 0.5F;

                Location location = player.getEyeLocation();
                location.setDirection(location.getDirection().multiply(20));

                player.getWorld().createExplosion(location, (radius*nLevel));

                player.setLastDamage(0);
                break;
            case 2:
                Location cloudLocation = player.getLocation().clone();
                cloudLocation.setY(cloudLocation.getY()+1);
                player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 250, 50));
                break;
            case 3:
                Location teleportSandLocation = player.getLocation().clone();
                teleportSandLocation.setY(teleportSandLocation.getY()-1);

                player.setVelocity(player.getLocation().getDirection().multiply(10.0F));
                break;
            case 4:
                if(player.getAllowFlight()){
                    player.setAllowFlight(false);
                    player.setFlying(false);
                    player.sendMessage(ChatColor.RED + "Flying deactivated");
                    player.removePotionEffect(PotionEffectType.INVISIBILITY);
                    return;
                }

                int level2 = player.getLevel();
                float speed = 0.2F;

                player.sendMessage(""+speed);

                player.setAllowFlight(true);
                player.setFlySpeed(speed);
                player.setFlying(true);
                player.sendMessage(ChatColor.GREEN + "Flying activated");
                player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 1000, 50));
                break;
        }
    }

    @Override
    public int strength() {
        return 9;
    }
}
