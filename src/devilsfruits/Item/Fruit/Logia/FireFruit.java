package devilsfruits.Item.Fruit.Logia;

import devilsfruits.Item.DevilFruit;
import devilsfruits.Item.Element.DevilFruitElement;
import devilsfruits.Item.Element.LogiaElement;
import devilsfruits.Item.Grade.DevilFruitGrade;
import devilsfruits.Item.Grade.FirstGrade;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Random;

public class FireFruit implements DevilFruit
{

    @Override
    public String name() {
        return "Fire Fruit";
    }

    @Override
    public DevilFruitElement element() {
        return new LogiaElement();
    }

    @Override
    public DevilFruitGrade grade() {
        return new FirstGrade();
    }

    @Override
    public void effect(Player player, Block block)
    {
        int slot = player.getInventory().getHeldItemSlot();

        int level = player.getLevel()+1;

        if(level > 50){
            level = 50;
        }

        switch (slot){
            case 0:
                for(int i = 0;i < level;++i){
                    Location location = player.getLocation().clone();
                    location.setY(location.getY()+1);
                    location.setX(location.getX()+(new Random()).nextInt(10));
                    location.setZ(location.getZ()+(new Random()).nextInt(10));
                    Entity snowball = player.getWorld().spawnEntity(location, EntityType.FIREBALL);
                    snowball.setVelocity(player.getLocation().getDirection().multiply(10.0F));
                }
                for(int i = 0;i < level;++i){
                    Location location = player.getLocation().clone();
                    location.setY(location.getY()+1);
                    location.setX(location.getX()+(new Random()).nextInt(10));
                    location.setZ(location.getZ()+(new Random()).nextInt(10));
                    Entity snowball = player.getWorld().spawnEntity(location, EntityType.FIREBALL);
                    snowball.setVelocity(player.getLocation().getDirection().multiply(10.0F));
                }
                break;
            case 1:
                Location location1 = player.getLocation().clone();
                location1.setY(location1.getY()+5);
                Entity fireball1 = player.getWorld().spawnEntity(location1, EntityType.FIREBALL);
                fireball1.setVelocity((fireball1.getVelocity().multiply(10.0)));
                break;
            case 2:
                for(int i = -3;i < ((level/2)+1);++i){
                    Location locationX = player.getLocation().clone();
                    locationX.setX(locationX.getX()+i);
                    if(
                            !locationX.getBlock().getType().equals(Material.AIR) &&
                            !locationX.getBlock().getType().equals(Material.GRASS) &&
                            !locationX.getBlock().getType().equals(Material.FERN) &&
                            !locationX.getBlock().getType().equals(Material.BROWN_MUSHROOM) &&
                            !locationX.getBlock().getType().equals(Material.RED_MUSHROOM) &&
                            !locationX.getBlock().getType().equals(Material.SUNFLOWER) &&
                            !locationX.getBlock().getType().equals(Material.SNOW) &&
                            !locationX.getBlock().getType().equals(Material.SNOW_BLOCK) &&
                            !locationX.getBlock().getType().toString().contains("_TULIP")
                    ){
                        continue;
                    }
                    locationX.getBlock().setType(Material.FIRE);
                }

                for(int i = -3;i < 3;++i){
                    Location locationZ = player.getLocation().clone();
                    locationZ.setZ(locationZ.getZ()+i);
                    if(
                            !locationZ.getBlock().getType().equals(Material.AIR) &&
                                    !locationZ.getBlock().getType().equals(Material.GRASS) &&
                                    !locationZ.getBlock().getType().equals(Material.FERN) &&
                                    !locationZ.getBlock().getType().equals(Material.BROWN_MUSHROOM) &&
                                    !locationZ.getBlock().getType().equals(Material.RED_MUSHROOM) &&
                                    !locationZ.getBlock().getType().equals(Material.SUNFLOWER) &&
                                    !locationZ.getBlock().getType().equals(Material.SNOW) &&
                                    !locationZ.getBlock().getType().equals(Material.SNOW_BLOCK) &&
                                    !locationZ.getBlock().getType().toString().contains("_TULIP")
                    ){
                        continue;
                    }
                    locationZ.getBlock().setType(Material.FIRE);

                }

                break;
            case 3:
                for(int y = 0;y < level;++y){
                    Location jumpLocation = player.getLocation().clone();
                    jumpLocation.setY(jumpLocation.getY()+y);

                    if(y%2 > 0){
                        jumpLocation.getBlock().setType(Material.FIRE);
                    }else{
                        jumpLocation.getBlock().setType(Material.NETHERRACK);
                    }
                }

                Location teleportLocation = player.getLocation().clone();
                teleportLocation.setY(teleportLocation.getY()+level+1);
                player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_FALLING, 100, 25));
                player.teleport(teleportLocation);
                break;
            case 4:
                Location location = player.getLocation().clone();
                location.setY(location.getY()+3);
                location.setX(location.getX()+(new Random()).nextInt(10));
                location.setZ(location.getZ()+(new Random()).nextInt(10));
                Entity snowball = player.getWorld().spawnEntity(location, EntityType.SMALL_FIREBALL);
                snowball.setVelocity(player.getLocation().getDirection().multiply(1.0F));
                snowball.addPassenger(player);
                break;
        }
    }

    @Override
    public int strength() {
        return 8;
    }

    @Override
    public boolean physicalImmunity() {
        return true;
    }
}
