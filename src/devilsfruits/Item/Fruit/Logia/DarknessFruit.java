package devilsfruits.Item.Fruit.Logia;

import devilsfruits.Item.DevilFruit;
import devilsfruits.Item.Element.DevilFruitElement;
import devilsfruits.Item.Element.LogiaElement;
import devilsfruits.Item.Grade.DevilFruitGrade;
import devilsfruits.Item.Grade.FirstGrade;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Random;

public class DarknessFruit implements DevilFruit
{
    @Override
    public String name() {
        return "Darkness Fruit";
    }

    @Override
    public DevilFruitElement element() {
        return new LogiaElement();
    }

    @Override
    public DevilFruitGrade grade() {
        return new FirstGrade();
    }

    @Override
    public void effect(Player player, Block block)
    {
        int slot = player.getInventory().getHeldItemSlot();

        int level = player.getLevel()+1;

        if(level > 50){
            level = 50;
        }

        switch (slot){
            case 0:
                for(int i = 0;i < level;i += 2){
                    Location location = player.getLocation().clone();
                    location.setY(location.getY()+1+(i*2));
                    Entity fireball = player.getWorld().spawnEntity(location, EntityType.WITHER_SKULL);
                    fireball.setVelocity((fireball.getVelocity().multiply(10.0)));
                }
                break;
            case 1:
                Location location2 = player.getLocation().clone();
                location2.setY(location2.getY()+5);
                Entity fireball2 = player.getWorld().spawnEntity(location2, EntityType.DRAGON_FIREBALL);
                fireball2.setVelocity((fireball2.getVelocity().multiply(10.0)));
                break;
            case 2:
                int i = 0;
                for(Entity entity:player.getNearbyEntities(player.getLocation().getX(), player.getLocation().getY(), player.getLocation().getZ())){
                    if(entity.getUniqueId().toString().equals(player.getUniqueId().toString())){
                        continue;
                    }

                    if(!(entity instanceof LivingEntity)){
                        continue;
                    }

                    if(i > level){
                        continue;
                    }

                    LivingEntity livingEntity = (LivingEntity) entity;

                    ((LivingEntity) entity).addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 100, 50));
                    ++i;
                }
                player.sendMessage(ChatColor.GREEN + "Wrapped near enemies into darkness.");
                break;
            case 3:
                Location location = player.getLocation().clone();
                location.setY(location.getY()+3);
                location.setX(location.getX()+(new Random()).nextInt(10));
                location.setZ(location.getZ()+(new Random()).nextInt(10));
                Entity snowball = player.getWorld().spawnEntity(location, EntityType.ENDER_PEARL);
                snowball.setVelocity(player.getLocation().getDirection().multiply(1.0F));
                snowball.addPassenger(player);
                break;
        }
    }

    @Override
    public int strength() {
        return 3;
    }
}
