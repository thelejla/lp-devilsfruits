package devilsfruits.Item.Fruit.Paramecia;

import devilsfruits.Item.DevilFruit;
import devilsfruits.Item.Element.DevilFruitElement;
import devilsfruits.Item.Element.LogiaElement;
import devilsfruits.Item.Element.ParameciaElement;
import devilsfruits.Item.Grade.DevilFruitGrade;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class SleepFruit implements DevilFruit {
    @Override
    public String name() {
        return "Sleep Fruit";
    }

    @Override
    public DevilFruitElement element() {
        return new ParameciaElement();
    }

    @Override
    public DevilFruitGrade grade() {
        return null;
    }

    @Override
    public void effect(Player player, Block block) {

    }

    @Override
    public boolean effectAtEntity(Player player, Entity entity) {
        int slot = player.getInventory().getHeldItemSlot();

        int level = player.getLevel()+1;

        if(level > 50){
            level = 50;
        }

        switch (slot){
            case 0:
                if(!(entity instanceof Player)){
                    return false;
                }

                ((Player) entity).addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 25*level, 50));
                ((Player) entity).addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 25*level, 50));
                ((Player) entity).addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 25*level, 50));
                player.sendMessage(ChatColor.GREEN + entity.getName() + " fall asleep.");
                break;
        }
        return true;
    }

    @Override
    public int strength() {
        return 67;
    }
}
