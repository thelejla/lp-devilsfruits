package devilsfruits.Item.Fruit.Paramecia;

import devilsfruits.Item.DevilFruit;
import devilsfruits.Item.Element.DevilFruitElement;
import devilsfruits.Item.Element.LogiaElement;
import devilsfruits.Item.Element.ParameciaElement;
import devilsfruits.Item.Grade.DevilFruitGrade;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class CopyFruit implements DevilFruit {
    @Override
    public String name() {
        return "Copy Fruit";
    }

    @Override
    public DevilFruitElement element() {
        return new ParameciaElement();
    }

    @Override
    public DevilFruitGrade grade() {
        return null;
    }

    @Override
    public void effect(Player player, Block block) {

    }

    @Override
    public int strength() {
        return 35;
    }
}
