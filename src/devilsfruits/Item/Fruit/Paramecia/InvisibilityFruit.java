package devilsfruits.Item.Fruit.Paramecia;

import devilsfruits.Item.DevilFruit;
import devilsfruits.Item.Element.DevilFruitElement;
import devilsfruits.Item.Element.LogiaElement;
import devilsfruits.Item.Element.ParameciaElement;
import devilsfruits.Item.Grade.DevilFruitGrade;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class InvisibilityFruit implements DevilFruit {
    @Override
    public String name() {
        return "Invisbility Fruit";
    }

    @Override
    public DevilFruitElement element() {
        return new ParameciaElement();
    }

    @Override
    public DevilFruitGrade grade() {
        return null;
    }

    @Override
    public void effect(Player player, Block block)
    {
        int level = player.getLevel();

        if(level > 25){
            level = 25;
        }

        player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 50*(level+1), 50));
    }

    @Override
    public int strength() {
        return 85;
    }
}
