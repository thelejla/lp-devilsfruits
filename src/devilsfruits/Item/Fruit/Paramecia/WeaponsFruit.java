package devilsfruits.Item.Fruit.Paramecia;

import devilsfruits.Item.DevilFruit;
import devilsfruits.Item.Element.DevilFruitElement;
import devilsfruits.Item.Element.LogiaElement;
import devilsfruits.Item.Element.ParameciaElement;
import devilsfruits.Item.Grade.DevilFruitGrade;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Random;

public class WeaponsFruit implements DevilFruit {
    @Override
    public String name() {
        return "Weapons Fruit";
    }

    @Override
    public DevilFruitElement element() {
        return new ParameciaElement();
    }

    @Override
    public DevilFruitGrade grade() {
        return null;
    }

    @Override
    public void effect(Player player, Block block) {
        int level = player.getLevel();
        String materialType = "DIAMOND";

        if(level < 9){ materialType = "WOODEN"; }
        else if(level <= 19){ materialType = "STONE"; }
        else if(level <= 29) { materialType = "IRON"; }
        else if(level <= 39) { materialType = "GOLDEN"; }

        int slot = player.getInventory().getHeldItemSlot();

        if(slot > 4){
            return;
        }

        if(slot <= 0){
            for(int i = 0;i < level;++i){
                if(i <= 0){
                    Location location = player.getLocation().clone();
                    location.setY(location.getY()+1);
                    Entity snowball = player.getWorld().spawnEntity(location, EntityType.ARROW);
                    snowball.setVelocity(player.getLocation().getDirection().multiply(10.0F));
                    continue;
                }
                Location location = player.getLocation().clone();
                location.setY(location.getY()+1);
                location.setX(location.getX()+(new Random()).nextInt(10));
                location.setZ(location.getZ()+(new Random()).nextInt(10));
                Entity snowball = player.getWorld().spawnEntity(location, EntityType.ARROW);
                snowball.setVelocity(player.getLocation().getDirection().multiply(10.0F));
            }
            for(int i = 0;i < level;++i){
                if(i <= 0){
                    Location location = player.getLocation().clone();
                    location.setY(location.getY()+1);
                    Entity snowball = player.getWorld().spawnEntity(location, EntityType.ARROW);
                    snowball.setVelocity(player.getLocation().getDirection().multiply(10.0F));
                    continue;
                }
                Location location = player.getLocation().clone();
                location.setY(location.getY()+1);
                location.setX(location.getX()+(new Random()).nextInt(10));
                location.setZ(location.getZ()+(new Random()).nextInt(10));
                Entity snowball = player.getWorld().spawnEntity(location, EntityType.ARROW);
                snowball.setVelocity(player.getLocation().getDirection().multiply(10.0F));
            }
            return;
        }

        String name = "SWORD";

        switch (slot){
            case 1:
                name = "SWORD";
                break;
            case 2:
                name = "AXE";
                break;
            case 3:
                name = "PICKAXE";
                break;
            case 4:
                name = "SHOVEL";
                break;
            case 5:
                name = "HOE";
                break;
        }

        ItemStack item = new ItemStack(Material.valueOf(materialType + "_" + name), 1);
        item.setAmount(1);
        item.addUnsafeEnchantment(Enchantment.QUICK_CHARGE, 1);
        item.addUnsafeEnchantment(Enchantment.KNOCKBACK, 2);
        item.addUnsafeEnchantment(Enchantment.DURABILITY, 3);
        item.addUnsafeEnchantment(Enchantment.BINDING_CURSE, 1);

        player.getInventory().setItemInMainHand(item);
        player.updateInventory();
    }

    @Override
    public int strength() {
        return 87;
    }
}
