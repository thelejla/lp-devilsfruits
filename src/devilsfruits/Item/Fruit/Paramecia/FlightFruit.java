package devilsfruits.Item.Fruit.Paramecia;

import devilsfruits.Item.DevilFruit;
import devilsfruits.Item.Element.DevilFruitElement;
import devilsfruits.Item.Element.LogiaElement;
import devilsfruits.Item.Element.ParameciaElement;
import devilsfruits.Item.Grade.DevilFruitGrade;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class FlightFruit implements DevilFruit {
    @Override
    public String name() {
        return "Flight Fruit";
    }

    @Override
    public DevilFruitElement element() {
        return new ParameciaElement();
    }

    @Override
    public DevilFruitGrade grade() {
        return null;
    }

    @Override
    public void effect(Player player, Block block) {
        int slot = player.getInventory().getHeldItemSlot();

        if(slot > 0){
            return;
        }

        if(!player.getInventory().getItemInMainHand().getType().equals(Material.AIR)){
            return;
        }


        if(player.getAllowFlight()){
            player.setAllowFlight(false);
            player.setFlying(false);
            player.sendMessage(ChatColor.RED + "Flying deactivated");
            return;
        }

        int level = player.getLevel();
        float speed = 0.1F;

        for (int i = 0;i <= level;++i){
            speed += 0.01F;
        }

        if(speed > 1.0F){
            speed = 1.0F;
        }

        player.sendMessage(""+speed);

        player.setAllowFlight(true);
        player.setFlySpeed(speed);
        player.setFlying(true);
        player.sendMessage(ChatColor.GREEN + "Flying activated");

    }

    @Override
    public int strength() {
        return 13;
    }
}
