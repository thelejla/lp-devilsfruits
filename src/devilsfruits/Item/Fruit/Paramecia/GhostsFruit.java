package devilsfruits.Item.Fruit.Paramecia;

import devilsfruits.Item.DevilFruit;
import devilsfruits.Item.Element.DevilFruitElement;
import devilsfruits.Item.Element.LogiaElement;
import devilsfruits.Item.Element.ParameciaElement;
import devilsfruits.Item.Grade.DevilFruitGrade;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class GhostsFruit implements DevilFruit {
    @Override
    public String name() {
        return "GhostsFruit";
    }

    @Override
    public DevilFruitElement element() {
        return new ParameciaElement();
    }

    @Override
    public DevilFruitGrade grade() {
        return null;
    }

    @Override
    public void effect(Player player, Block block) {

        if(!player.getInventory().getItemInMainHand().getType().equals(Material.AIR)){
            return;
        }

        int slot = player.getInventory().getHeldItemSlot();

        switch (slot){
            case 0:
                if(player.isCollidable()){
                    player.setCollidable(false);
                    player.sendMessage(ChatColor.RED + "Ghost deactivated");
                    return;
                }

                player.setCollidable(true);
                player.sendMessage(ChatColor.GREEN + "Ghost activated");
                break;
            case 1:
                player.addPotionEffect(new PotionEffect(PotionEffectType.LEVITATION, 10*(player.getLevel()+1) ,50));
                player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_FALLING, 50*(player.getLevel()+1)+100 ,50));
                player.sendMessage(ChatColor.GREEN + "Ghost leviatation");

                break;
        }

    }

    @Override
    public int strength() {
        return 16;
    }
}
