package devilsfruits.Item.Fruit.Paramecia;

import devilsfruits.Item.DevilFruit;
import devilsfruits.Item.Element.ParameciaElement;
import devilsfruits.Item.Element.DevilFruitElement;
import devilsfruits.Item.Grade.DevilFruitGrade;
import devilsfruits.Item.Grade.ThirdGrade;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class GumGumFruit implements DevilFruit
{
    @Override
    public String name() {
        return "Gum Gum Fruit";
    }

    @Override
    public DevilFruitElement element() {
        return new ParameciaElement();
    }

    @Override
    public DevilFruitGrade grade() {
        return new ThirdGrade();
    }

    @Override
    public void effect(Player player, Block block) {

    }

    @Override
    public int strength() {
        return 6;
    }
}
