package devilsfruits.Item.Fruit.Paramecia;

import devilsfruits.Item.DevilFruit;
import devilsfruits.Item.Element.DevilFruitElement;
import devilsfruits.Item.Element.LogiaElement;
import devilsfruits.Item.Element.ParameciaElement;
import devilsfruits.Item.Grade.DevilFruitGrade;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class HealingFruit implements DevilFruit {
    @Override
    public String name() {
        return "Healing Fruit";
    }

    @Override
    public DevilFruitElement element() {
        return new ParameciaElement();
    }

    @Override
    public DevilFruitGrade grade() {
        return null;
    }

    @Override
    public void effect(Player player, Block block) {
        //TODO: implement heal other players
        int slot = player.getInventory().getHeldItemSlot();
        int level = player.getLevel()+1;

        if(level > 100){
            level = 100;
        }

        level = level/4;


        switch (slot){
            case 0:
                player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, (level*50)+100, 50));
                break;
            case 1:
                for(Entity nearEntity:player.getNearbyEntities(player.getLocation().getX(), player.getLocation().getY(), player.getLocation().getZ())){
                    if(nearEntity.getUniqueId().toString().equals(player.getUniqueId().toString())){
                        continue;
                    }

                    player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, (level*50)+100, 50));
                }
                break;
        }
    }

    @Override
    public int strength() {
        return 22;
    }
}
