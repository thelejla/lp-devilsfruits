package devilsfruits.Item.Fruit.Zoan;

import devilsfruits.Item.DevilFruit;
import devilsfruits.Item.Element.DevilFruitElement;
import devilsfruits.Item.Element.LogiaElement;
import devilsfruits.Item.Element.ZoanElement;
import devilsfruits.Item.Grade.DevilFruitGrade;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class BirdFruit implements DevilFruit {
    @Override
    public String name() {
        return "BirdFruit Fruit";
    }

    @Override
    public DevilFruitElement element() {
        return new ZoanElement();
    }

    @Override
    public DevilFruitGrade grade() {
        return null;
    }

    @Override
    public void effect(Player player, Block block) {

    }

    @Override
    public int strength() {
        return 15;
    }
}
