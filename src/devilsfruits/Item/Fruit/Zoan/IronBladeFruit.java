package devilsfruits.Item.Fruit.Zoan;

import devilsfruits.Item.DevilFruit;
import devilsfruits.Item.Element.DevilFruitElement;
import devilsfruits.Item.Element.ZoanElement;
import devilsfruits.Item.Grade.DevilFruitGrade;
import devilsfruits.Item.Grade.ThirdGrade;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class IronBladeFruit implements DevilFruit
{
    @Override
    public String name()
    {
        return "Iron Blade Fruit";
    }

    @Override
    public DevilFruitElement element()
    {
        return new ZoanElement();
    }

    @Override
    public DevilFruitGrade grade()
    {
        return new ThirdGrade();
    }

    @Override
    public void effect(Player player, Block block)
    {

    }

    @Override
    public int strength()
    {
        return 4;
    }
}
