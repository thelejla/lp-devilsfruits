package devilsfruits;

import devilsfruits.Command.DevilExchangeCommand;
import devilsfruits.Command.FruitCommand;
import devilsfruits.Item.Storage.DevilFruitStorage;
import devilsfruits.Item.Storage.FruitOwnerStorage;
import devilsfruits.Listener.*;
import devilsfruits.TabCompleter.FruitTabCompleter;
import org.bukkit.plugin.java.JavaPlugin;
import recipes.Recipes;

public class Main extends JavaPlugin
{
    @Override
    public void onEnable()
    {
        //TODO: add gold exchanger for fruits that could be selled for $ in an economic plugin
        //TODO: fruit owners overvier for web and ingame
        //TODO: bounty
        //TODO: /ultime to perform ultimate attack
        this.saveConfig();

        DevilFruitStorage devilFruitStorage = new DevilFruitStorage(this.getDataFolder().getPath());
        FruitOwnerStorage fruitOwnerStorage = new FruitOwnerStorage(this.getDataFolder().getPath());

        this.getCommand("exchange").setExecutor(new DevilExchangeCommand());

        this.getCommand("fruit").setExecutor(new FruitCommand());
        this.getCommand("fruit").setTabCompleter(new FruitTabCompleter());

        this.getServer().getPluginManager().registerEvents(new FruitDropListener(fruitOwnerStorage), this);
        this.getServer().getPluginManager().registerEvents(new FruitConsumeListener(devilFruitStorage), this);
        this.getServer().getPluginManager().registerEvents(new FruitPhysicalImmunityListener(devilFruitStorage), this);
        this.getServer().getPluginManager().registerEvents(new FruitUseListener(devilFruitStorage), this);
        this.getServer().getPluginManager().registerEvents(new FruitMoveListener(devilFruitStorage), this);
        this.getServer().getPluginManager().registerEvents(new FruitOwnerDeathListener(devilFruitStorage), this);
        this.getServer().getPluginManager().registerEvents(new SeaStoneProjectileListener(devilFruitStorage), this);

        //Recipes
        Recipes recipes = new Recipes(this);
        recipes.init();
    }

}
