package devilsfruits.Listener;

import devilsfruits.Item.Creator.DevilFruitCreator;
import devilsfruits.Item.DevilFruit;
import devilsfruits.Item.Fruits;
import devilsfruits.Item.Storage.FruitOwnerStorage;
import org.bukkit.ChatColor;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.entity.Item;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;

import java.io.IOException;
import java.util.List;
import java.util.Random;

public class FruitDropListener implements Listener
{
    private FruitOwnerStorage storage;

    public FruitDropListener(FruitOwnerStorage storage)
    {
        this.storage = storage;
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onMonsterDeath(EntityDeathEvent event)
    {
       if (!(event.getEntity() instanceof Monster)){
           return;
       }

       Random random = new Random();
       //TODO: change this to lower 10%
       if ((random.nextInt(100)+1) > 10){
           return;
       }

        List<DevilFruit> fruits = Fruits.fruits();

        DevilFruit devilFruit = fruits.get(random.nextInt(fruits.size()));

        if(this.storage.isOwned(devilFruit)){
            return;
        }

       //TODO: add more fruits and drop random fruits
        //TODO: check if fruit is in use and limit them
       ItemStack itemStack = DevilFruitCreator.create(devilFruit);

       if(null == itemStack){
           return;
       }

       Item item = event.getEntity().getWorld().dropItem(event.getEntity().getLocation(), itemStack);
       item.setCustomNameVisible(true);
       item.setCustomName(ChatColor.DARK_RED + "Devilfruit");
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onFruitPickUp(EntityPickupItemEvent event) throws IOException, InvalidConfigurationException {
        if(!(event.getEntity() instanceof Player)){
            return;
        }

        Player player = (Player) event.getEntity();

        DevilFruit fruit = Fruits.getFruitByItem(event.getItem().getItemStack());

        if(null == fruit){
            return;
        }

        if(this.storage.isOwned(fruit)){
            return;
        }

        if(!this.storage.store(player, fruit, false)){
            return;
        }

        player.getServer().broadcastMessage(ChatColor.DARK_RED + "" + ChatColor.MAGIC + player.getName() + " got a devil fruit.");
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onFruitDrop(PlayerDropItemEvent event) throws IOException, InvalidConfigurationException {
        Player player = event.getPlayer();

        DevilFruit fruit = Fruits.getFruitByItem(event.getItemDrop().getItemStack());

        if(null == fruit){
            return;
        }

        if(!this.storage.isOwned(fruit)){
            return;
        }

        if(!this.storage.remove(fruit)){
            return;
        }

        player.getServer().broadcastMessage(ChatColor.DARK_RED + "" + ChatColor.MAGIC + player.getName() + " lost a devil fruit.");
    }
}
