package devilsfruits.Listener;

import devilsfruits.Item.DevilFruit;
import devilsfruits.Item.Storage.DevilFruitStorage;

import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerPickupArrowEvent;
import org.bukkit.event.player.PlayerUnleashEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import recipes.recipe.SeaStoneArrow;


public class SeaStoneProjectileListener implements Listener
{
    private DevilFruitStorage storage;

    public SeaStoneProjectileListener(DevilFruitStorage storage)
    {
        this.storage = storage;
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onArrowShoot(ProjectileLaunchEvent event)
    {
        if(null == event.getEntity().getShooter()){
            return;
        }

        if(!(event.getEntity().getShooter() instanceof Player)){
            return;
        }

        Player player = (Player) event.getEntity().getShooter();
        Arrow arrow = (Arrow) event.getEntity();

        SeaStoneArrow seaStoneArrow = new SeaStoneArrow();

        int i = -1;
        int slot = -1;

        for(ItemStack pItem:player.getInventory().getContents()){
            ++i;
            if(null == pItem){
                continue;
            }

            if(null == pItem.getItemMeta()){
                continue;
            }

            if(!pItem.getItemMeta().getDisplayName().equals(seaStoneArrow.name())){
                continue;
            }
            slot = i;
            break;
        }

        if(slot < 0){
            return;
        }



        ItemStack itemStack = player.getInventory().getItem(slot);

        if(null == itemStack){
            return;
        }

        if(itemStack.getAmount() <= 1){
            player.getInventory().clear(slot);
            player.getInventory().addItem(new ItemStack(Material.ARROW, 1));
            player.updateInventory();
        }else {
            itemStack.setAmount(itemStack.getAmount()-1);
            player.getInventory().setItem(slot, itemStack);
            player.getInventory().addItem(new ItemStack(Material.ARROW, 1));
            player.updateInventory();
        }

        arrow.setCustomNameVisible(true);
        arrow.setCustomName(seaStoneArrow.name());
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onSeaStoneArrowHit(ProjectileHitEvent event)
    {
        if(!(event.getHitEntity() instanceof Player)){
            return;
        }

        Player target = (Player) event.getHitEntity();

        if(!(event.getEntity() instanceof Arrow)){
            return;
        }

        Arrow arrow = (Arrow) event.getEntity();

        if(!arrow.isCustomNameVisible()){
            return;
        }

        if(null == arrow.getCustomName()){
            return;
        }

        SeaStoneArrow seaStoneArrow = new SeaStoneArrow();

        if(!arrow.getCustomName().equals(seaStoneArrow.name())){
            return;
        }

        DevilFruit devilFruit = this.storage.getFruit(target);

        if(null == devilFruit){
            arrow.setDamage(0);
            return;
        }

        if(target.getLevel() > 0){
            target.setLevel(target.getLevel()-1);
        }

        target.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 100, 25));
        target.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 250, 50));
        target.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 100, 25));
        target.getWorld().spawnParticle(Particle.WATER_BUBBLE, target.getLocation(), 10);
    }
}
