package devilsfruits.Listener;

import devilsfruits.Item.DevilFruit;
import devilsfruits.Item.Storage.DevilFruitStorage;
import org.bukkit.ChatColor;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import java.io.IOException;

public class FruitOwnerDeathListener implements Listener
{
    private DevilFruitStorage storage;

    public FruitOwnerDeathListener(DevilFruitStorage storage)
    {
        this.storage = storage;
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onOwnerDeath(PlayerDeathEvent event)
    {
        DevilFruit fruit = this.storage.getFruit(event.getEntity());

        if(null == fruit){
            return;
        }

        try {
            this.storage.remove(event.getEntity());
            event.getEntity().setAllowFlight(false);
            event.getEntity().setFlying(false);
            event.getEntity().setFlySpeed(1.0F);
            event.getEntity().setCollidable(true);
            event.getEntity().sendMessage(ChatColor.RED + "You have lost your devilsfruit power, the fruit can respawn again now.");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        }
        //TODO: check if fruit is already gained by a player (add fruit to a separate list)
        event.setDeathMessage("");
    }
}
