package devilsfruits.Listener;

import com.sun.javafx.tk.quantum.MasterTimer;
import devilsfruits.Item.DevilFruit;
import devilsfruits.Item.Fruits;
import devilsfruits.Item.Storage.DevilFruitStorage;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.IOException;
import java.util.List;

public class FruitConsumeListener implements Listener
{
    private DevilFruitStorage storage;

    public FruitConsumeListener(DevilFruitStorage storage)
    {
        this.storage = storage;
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onFruitConsume(PlayerInteractEvent event)
    {
        if(null == event.getItem()){
            return;
        }

        DevilFruit fruit = Fruits.getFruitByItem(event.getItem());

        if(null == fruit){
            return;
        }

        try {
            //TODO: add force store to devilfruitstorage
            if(!this.storage.forceStore(event.getPlayer(), fruit)){
                event.getPlayer().sendMessage(ChatColor.RED + "You already owned the power of another devilsfruit.");
                return;
            }

            event.setCancelled(true);
            event.getPlayer().sendMessage(ChatColor.GREEN + "You owned the power of " + fruit.element().color() + fruit.name() + ChatColor.GREEN + " now.");
            event.getPlayer().getInventory().setItemInMainHand(new ItemStack(Material.AIR, 1));
            event.getPlayer().updateInventory();
            return;
        }catch (InvalidConfigurationException e) {
            e.printStackTrace();

            return;
        } catch (IOException e) {
            e.printStackTrace();

            return;
        }
    }
}
