package devilsfruits.Listener;

import devilsfruits.Item.DevilFruit;
import devilsfruits.Item.Storage.DevilFruitStorage;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class FruitUseListener implements Listener
{
    private DevilFruitStorage storage;

    public FruitUseListener(DevilFruitStorage storage)
    {
        this.storage = storage;
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onInteraction(PlayerInteractEvent event)
    {
        if(!event.getPlayer().getInventory().getItemInMainHand().getType().equals(Material.AIR)){
            return;
        }

        Player player = event.getPlayer();

        DevilFruit fruit = this.storage.getFruit(player);

        if(null == fruit){
            return;
        }

        fruit.effect(player, event.getClickedBlock());
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onInteraction(PlayerInteractAtEntityEvent event)
    {
        if(!event.getPlayer().getInventory().getItemInMainHand().getType().equals(Material.AIR)){
            return;
        }

        if(!(event.getRightClicked() instanceof Player)){
            return;
        }

        Player target = (Player) event.getRightClicked();
        Player player = event.getPlayer();

        DevilFruit fruit = this.storage.getFruit(player);

        if(null == fruit){
            return;
        }

        boolean effected = fruit.effectAtEntity(player, target);
        if(!effected){
            return;
        }
        event.setCancelled(true);
    }

}
