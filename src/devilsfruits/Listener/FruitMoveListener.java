package devilsfruits.Listener;

import devilsfruits.Item.DevilFruit;
import devilsfruits.Item.Storage.DevilFruitStorage;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;

public class FruitMoveListener implements Listener
{
    private DevilFruitStorage storage;

    public FruitMoveListener(DevilFruitStorage storage)
    {
        this.storage = storage;
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onMove(PlayerMoveEvent event)
    {
        if(!event.getPlayer().getInventory().getItemInMainHand().getType().equals(Material.AIR)){
            return;
        }

        Player player = event.getPlayer();

        DevilFruit fruit = this.storage.getFruit(player);

        if(null == fruit){
            return;
        }

        fruit.move(player);
    }
}
