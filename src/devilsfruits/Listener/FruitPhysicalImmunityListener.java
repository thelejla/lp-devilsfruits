package devilsfruits.Listener;

import devilsfruits.Item.DevilFruit;
import devilsfruits.Item.Element.LogiaElement;
import devilsfruits.Item.Fruit.Logia.*;
import devilsfruits.Item.Fruit.Paramecia.WeaponsFruit;
import devilsfruits.Item.Storage.DevilFruitStorage;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Utility;
import org.bukkit.block.Block;
import org.bukkit.block.data.type.Fire;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;

public class FruitPhysicalImmunityListener implements Listener
{

    private DevilFruitStorage storage;

    public FruitPhysicalImmunityListener(DevilFruitStorage storage)
    {
        this.storage = storage;
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerDamage(EntityDamageByEntityEvent event)
    {
        if(!(event.getEntity() instanceof Player)){
            return;
        }

        Player player = (Player) event.getEntity();

        DevilFruit fruit = this.storage.getFruit(player);

        if(null == fruit){
            return;
        }

        if(!(fruit.element() instanceof LogiaElement)){
            return;
        }

        Entity damager = event.getDamager();

        if(
                (damager instanceof Explosive) ||
                (damager instanceof Firework) ||
                (damager instanceof FallingBlock) ||
                (damager instanceof LightningStrike) ||
                (damager instanceof Slime) ||
                (damager instanceof Snowball) ||
                (damager instanceof Spellcaster) ||
                (damager instanceof SpectralArrow) ||
                (damager instanceof ThrownPotion)
        ){
            return;
        }

        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onDamageByFire(EntityDamageEvent event)
    {
        if(!(event.getEntity() instanceof Player)){
            return;
        }

        Player player = (Player) event.getEntity();

        DevilFruit fruit = this.storage.getFruit(player);

        if(null == fruit){
            return;
        }

        EntityDamageEvent.DamageCause cause = event.getCause();

        if(
                cause.equals(EntityDamageEvent.DamageCause.FALL)
        ){
            event.setCancelled(true);
            player.setLastDamage(0);
            return;
        }

        if(
                cause.equals(EntityDamageEvent.DamageCause.LIGHTNING)
        ){
            if(fruit instanceof ThunderFruit){
                player.setLastDamage(0);
                event.setCancelled(true);
                return;
            }
        }

        if(
                cause.equals(EntityDamageEvent.DamageCause.LAVA) ||
                cause.equals(EntityDamageEvent.DamageCause.FIRE_TICK) ||
                cause.equals(EntityDamageEvent.DamageCause.HOT_FLOOR) ||
                cause.equals(EntityDamageEvent.DamageCause.FIRE)
        ){
            if(fruit instanceof MagmaFruit){
                player.setLastDamage(0);
                event.setCancelled(true);
                return;
            }
        }

        if(
                cause.equals(EntityDamageEvent.DamageCause.FIRE) ||
                cause.equals(EntityDamageEvent.DamageCause.FIRE_TICK) ||
                cause.equals(EntityDamageEvent.DamageCause.HOT_FLOOR)
        ){
            if(fruit instanceof FireFruit){
                player.setLastDamage(0);
                event.setCancelled(true);
            }
        }

        if(cause.equals(EntityDamageEvent.DamageCause.PROJECTILE)){
            if(fruit instanceof WeaponsFruit){
                player.setLastDamage(0);
                event.setCancelled(true);
            }
        }

        if(
                cause.equals(EntityDamageEvent.DamageCause.FALLING_BLOCK) ||
                cause.equals(EntityDamageEvent.DamageCause.FLY_INTO_WALL)
        ){
            if(fruit instanceof SandFruit){
                player.setLastDamage(0);
                event.setCancelled(true);
            }
        }

        if(cause.equals(EntityDamageEvent.DamageCause.ENTITY_EXPLOSION) || cause.equals(EntityDamageEvent.DamageCause.BLOCK_EXPLOSION)){
            if(fruit instanceof SmokeFruit){
                player.setLastDamage(0);
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onWaterInteraction(PlayerMoveEvent event)
    {
        Location location = event.getFrom().clone();

        if(!location.getBlock().getType().equals(Material.WATER)){
            return;
        }

        Player player = event.getPlayer();

        DevilFruit fruit = this.storage.getFruit(player);

        if(null == fruit){
            return;
        }

        if(null == event.getTo()){
            return;
        }

        if(
           event.getFrom().getY() > event.getTo().getY()
        ){
            return;
        }

        event.setCancelled(true);
    }
}
